package com.customer.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.customer.demo.model.Customer;
import com.customer.demo.service.CustomerService;

@RestController
public class CustomerController {
	@Autowired
	CustomerService customerService;
	
	
	@PostMapping("/saveDetails")
	public Customer saveDetails(@RequestBody Customer customer) {
		
		return customerService.saveDetails(customer);
	}
	

}
