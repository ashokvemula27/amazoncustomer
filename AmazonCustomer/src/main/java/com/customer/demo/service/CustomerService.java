package com.customer.demo.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.customer.demo.model.Customer;
import com.customer.demo.repository.CustomerRepository;

@Service
public class CustomerService {
	@Autowired
	CustomerRepository customeRepository;
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	@Autowired
	RestTemplate restTemplate;

	public Customer saveDetails(Customer customer) {
		
		String url= "http://localhost:8092/saveVendorDetails";
		Customer cus = restTemplate.getForObject(url, Customer.class );
		customer.setVendorId(cus.getVendorId());
		return customeRepository.save(customer);
	}

}
